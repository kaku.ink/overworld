import 'babel-polyfill'


import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:overworld:index')


import SocketIoServer from 'socket.io'
import * as redis from 'then-redis'
import * as _ from 'lodash'

import server from './http'
import { Map2D, bBoxIntersect } from '@kaku/common/utils'

import * as sharedConsts from '@kaku/common/consts'
import * as sharedLines from '@kaku/common/lines'
import { screenToTile, tileInBoundingBox } from '@kaku/common/tiles'


const ioServer = SocketIoServer(server)

const redisOptions = {
  host: process.env.KAKU_REDIS_ADDRESS,
  port: process.env.KAKU_REDIS_PORT,
  password: process.env.KAKU_REDIS_PASSWORD
}

const redisClient = redis.createClient(redisOptions)
const redisClientSub = redis.createClient(redisOptions)


let players = []
const tileTimestamps = new Map2D()
let queuedPaints = []

function getJoinedPlayers() {
  return players.filter((p) => p._joined)
}


class Player {
  constructor(id) {
    this.id = id

    this.name = null
    this.color = '#000000'
    this.position = [0, 0]
    this.painting = false
    this.size = 8
    this.line = []
    this.tileTimestamps = new Map2D()

    this.scale = 1
    this.viewSize = [0, 0]
    this.pan = [0, 0]

    this._joined = false
    this._lastScale = this.scale
    this._lastPan = this._lastPan
    this._needsStaleCheck = true
    this._previewLines = []
  }

  get scaledViewSize() {
    return [
      Math.ceil(this.viewSize[0] / this.scale),
      Math.ceil(this.viewSize[1] / this.scale)
    ]
  }

  get tileViewSize() {
    return [
      Math.ceil(this.scaledViewSize[0] / sharedConsts.TILE_SIZE) *
        sharedConsts.TILE_SIZE,
      Math.ceil(this.scaledViewSize[1] / sharedConsts.TILE_SIZE) *
        sharedConsts.TILE_SIZE
    ]
  }

  get scaledViewBBox() {
    return {
      top: this.pan[1] - (this.scaledViewSize[1] / 2),
      bottom: this.pan[1] + (this.scaledViewSize[1] / 2),
      left: this.pan[0] - (this.scaledViewSize[0] / 2),
      right: this.pan[0] + (this.scaledViewSize[0] / 2)
    }
  }

  tileInViewRange(tilePosition) {
    return tileInBoundingBox(tilePosition, this.pan, this.tileViewSize)
  }

  guessTilesInRange() {
    const tiles = []

    const viewOrigin = [
      Math.floor(this.pan[0] - this.tileViewSize[0] / 2),
      Math.floor(this.pan[1] - this.tileViewSize[1] / 2)
    ]

    const viewEnd = [
      Math.ceil(this.pan[0] + this.tileViewSize[0] / 2),
      Math.ceil(this.pan[1] + this.tileViewSize[1] / 2)
    ]

    for (let y = viewOrigin[1]; y <= viewEnd[1]; y += sharedConsts.TILE_SIZE)
      for (let x = viewOrigin[0]; x <= viewEnd[0]; x += sharedConsts.TILE_SIZE)
        tiles.push(screenToTile([x, y]))

    return tiles
  }

  sendTiles(positions) {
    for (const position of positions) {
      this.tileTimestamps.set(position, tileTimestamps.get(position))
    }

    ioServer.to(this.id).emit('sync-tiles', { positions })
  }
}


ioServer.on('connection', (socket) => {
  let player = null;

  socket.on('init', (data, respond) => {
    player = new Player(socket.id)
    player.viewSize = data.viewSize
    players.push(player)

    debug('%s connected with viewsize: %s', player.id, player.viewSize)

    respond({
      id: player.id,
      players: getJoinedPlayers()
    })

    socket.on('sync', (data) => {
      player.position = data.position

      player.color = data.color
      player.painting = data.painting
      player.line = data.line
      player.size = data.size

      player._lastPan = player.pan
      player._lastScale = player.scale
      player.pan = data.pan
      player.scale = data.scale

      if (!_.isEqual(player._lastPan, player.pan) ||
          player.scale < player._lastScale)
        player._needsStaleCheck = true
    })

    socket.on('sync-resize', (data) => {
      player.viewSize = data.viewSize
    })
  })

  socket.on('join', (data) => {
    player.name = data.name
    player._joined = true

    socket.broadcast.emit('player-connected', {
      id: player.id,
      color: player.color,
      name: player.name
    })

    socket.on('sync-name', (data) => {
      player.name = data.name
    })

    socket.on('paint', (data, respond) => {
      player.line = data.line
      player.color = data.color
      player.painting = false
      player.line = []

      redisClient.incr('tiles.paintId')
        .then((id) => {
          id = Number.parseInt(id)
          data.id = id
          respond({ id })

          const lineBBox = sharedLines.getLineBoundingBox(data.line)
          for (const otherPlayer of players) {
            if (otherPlayer === player) {
              otherPlayer._previewLines.push(id)
            } else {
              if (bBoxIntersect(lineBBox, otherPlayer.scaledViewBBox)) {
                otherPlayer._previewLines.push(id)
                ioServer.to(otherPlayer.id).emit('paint', data)
              }
            }
          }

          redisClient.lpush('tiles.incoming', JSON.stringify({
            id,
            points: data.line,
            color: data.color
          }))
        })
        .catch(debug)
    })

    socket.on('chat', (data) => {
      let messageClean = data.message.trim()
      messageClean.substring(0, 256)

      socket.broadcast.emit('chat', {
        name: player.name,
        message: messageClean
      })
    })
  })

  socket.on('disconnect', () => {
    if (player && player._joined)
      socket.broadcast.emit('player-disconnected', { id: socket.id })

    players = players.filter((player) => { return player.id !== socket.id })
  })
})


setInterval(() => {
  ioServer.volatile.emit('sync', { players: getJoinedPlayers() })
}, sharedConsts.REMOTE_SYNC_TIME)

setInterval(() => {
  for (const player of players) {
    const previews = []
    const positions = []

    for (const paint of queuedPaints) {
      const i = player._previewLines.findIndex((p) => p === paint.id)
      if (i >= 0) {
        previews.push(paint.id)
        player._previewLines.splice(i, 1)

        for (const position of paint.positions) {
          if (!positions.find((p) => _.isEqual(p, position)))
            positions.push(position)
        }
      }
    }

    if (previews.length > 0 && positions.length > 0) {
      for (const position of positions) {
        player.tileTimestamps.set(position, tileTimestamps.get(position))
      }

      ioServer.to(player.id).emit('paint-done', {
        ids: previews,
        positions: positions
      })
    } else {
      player._needsStaleCheck = true
    }
  }

  queuedPaints = []

  for (const player of players) {
    if (!player._needsStaleCheck)
      continue

    const tilesInRange = player.guessTilesInRange()
    const staleTiles = tilesInRange.filter((position) => {
      return tileTimestamps.get(position) !== undefined && (
        player.tileTimestamps.get(position) === undefined ||
        player.tileTimestamps.get(position) < tileTimestamps.get(position))
    })

    if (staleTiles.length > 0) {
      player.sendTiles(staleTiles)
    }

    player._needsStaleCheck = false
  }
}, 1000)


redisClient.smembers('tiles')
  .then((redisData) => {
    const positions = redisData.map(JSON.parse)
    for (const position of positions) {
      tileTimestamps.set(position, 0)
    }
  })

redisClientSub.subscribe('tiles.paint-done-channel')
redisClientSub.on('message', (channel, message) => {
  const data = JSON.parse(message)
  for (const position of data.positions) {
    tileTimestamps.set(position, (tileTimestamps.get(position) || 0) + 1)
    queuedPaints.push(data)
  }
})

process.once('SIGUSR2', () => {
  _gracefulShutdown()
  process.kill(process.pid, 'SIGUSR2')
})

process.once('SIGTERM', () => {
  _gracefulShutdown()
  process.kill(process.pid, 'SIGTERM')
})

function _gracefulShutdown() {
  ioServer.emit('chat', {
    name: 'OVERWORLD',
    message: 'Kaku is shutting down, please refresh in a moment.'
  })
}
