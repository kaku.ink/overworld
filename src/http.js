import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:overworld:http')

import * as path from 'path'
import express from 'express'
import http from 'http'


const app = express()
const server = http.createServer(app)

if (process.env.NODE_ENV !== 'production') {
  app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../frontend/bin/index.html'))
  })
  app.get('/bundle.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../frontend/bin/bundle.js'))
  })

  app.get('/tiles/:filename', (req, res) => {
    res.sendFile(
      path.resolve(__dirname, '../../', process.env.KAKU_DATA_DIR,
        req.params.filename),
      { lastModified: false, headers: { 'Cache-Control': 'no-cache' } }
    )
  })
}

server.listen(
  process.env.KAKU_OVERWORLD_PORT,
  process.env.KAKU_OVERWORLD_ADDRESS)
debug('http listening on %s:%d',
  process.env.KAKU_OVERWORLD_ADDRESS,
  process.env.KAKU_OVERWORLD_PORT)


export default server
